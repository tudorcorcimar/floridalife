<?php

use Phinx\Migration\AbstractMigration;

class UserTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $user = $this->table('user');
        $user->addColumn('password', 'string', array('limit' => 255))
             ->addColumn('remember_token', 'string', array('limit' => 100, 'null' => true))
             ->addColumn('email', 'string', array('limit' => 100))
             ->addColumn('first_name', 'string', array('limit' => 150, 'null' => true))
             ->addColumn('last_name', 'string', array('limit' => 150, 'null' => true))
             ->addColumn('created_at', 'datetime')
             ->addColumn('updated_at', 'datetime', array('null' => true))
             ->addColumn('active', 'boolean', array('default' => false))
             ->addColumn('banned', 'boolean', array('default' => false))
             ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE user");
    }
}
